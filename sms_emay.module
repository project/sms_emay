<?php

/**
 * @file
 * Provide EMAY(http://www.emay.cn/) integration for SMS framework.
 */

/**
 * Implements hook_gateway_info().
 */
function sms_emay_gateway_info() {
  return array(
    'sms_emay' => array(
      'name' => 'SMS EMAY',
      'send' => 'sms_emay_send',
      'receive' => TRUE,
      'configure form' => 'sms_emay_admin_form',
      // 'send form' => 'sms_emay_send_form',
    )
  );
}

/**
 * Implements hook_send().
 */
function sms_emay_send($number, $message, $options = array()) {
  $status = FALSE;
  $return_message = t("No message sent");
  $validation_message = t("No validation attempted");

  // Validate the message length
  if (strlen($message) > 160) {
    $return_message = t('The message you wanted to send is too long and cannot be sent.');
    return array(
      'status' => $status,
      'message' => $return_message,
      'variables' => NULL,
      'validation' => $validation_message,
    );
  }

  $data = array(
    'phone' => trim($number),
    'message' => trim($message),
  );

  $command = '/sdkproxy/sendsms.action';
  $result = _sms_emay_gateway_request($command, $data);

  if(!isset($result->error)) {
    $xml = simplexml_load_string(trim($result->data));

    watchdog('sms_emay', '<pre >Log sended sms: <br />'. print_r($result->request, TRUE) . ' <br /> '. print_r($xml, TRUE)) . '</pre>';

    $status = TRUE;
    $return_message = t('Send success');
  }

  $result = array(
    'status' => $status,
    'message' => $return_message,
    'variables' => NULL,
  );

  return $result;
}

/**
 * Callback: sms_emay_admin_form.
 */
function sms_emay_admin_form($configuration) {
  $form = array();

  $form['sms_emay_url'] = array(
    '#type' => 'textfield',
    '#title' => t('EMAY GATEWAY URL'),
    '#default_value' => isset($configuration['sms_emay_url']) ? $configuration['sms_emay_url'] : '',
    '#description' => t('The custom URL used to access your EMAY GATEWAY API. It often looks like "http://www.emay.cn".'),
    '#required' => TRUE,
  );

  $form['sms_emay_cdkey'] = array(
    '#type' => 'textfield',
    '#title' => t('EMAY CDKEY'),
    '#default_value' => isset($configuration['sms_emay_cdkey']) ? $configuration['sms_emay_cdkey'] : '',
    '#description' => t('The EMAY CDKEY.'),
    '#required' => TRUE,
  );

  $form['sms_emay_password'] = array(
    '#type' => 'password',
    '#title' => t('EMAY Password'),
    '#description' => t('The password to access EMAY'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function sms_emay_form_sms_admin_gateway_form_alter(&$form, &$form_state) {
  $form['#validate'][] = 'sms_emay_admin_form_validate';
}

/**
 * Validate the admin form.
 */
function sms_emay_admin_form_validate($form, &$form_state) {
  // Register the CDKEY.
  $command = '/sdkproxy/regist.action';

  $url = $form_state['values']['sms_emay_url'];
  $data = array(
    'cdkey' => $form_state['values']['sms_emay_cdkey'],
    'password' => $form_state['values']['sms_emay_password'],
  );

  $params = drupal_http_build_query($data);
  $url = $url. $command . '?' . $params;
  $result = drupal_http_request($url, array('method'=>'GET', 'timeout'=>10));

  if(isset($result->error)) {
    form_set_error('', t('Register failure.'));
  } else {
    $xml = simplexml_load_string(trim($result->data));
    if ($xml->error < 0 && $xml->error != -104) {
      form_set_error('', t('Register failure, ERROR: ') . $xml->error);
    }
  }
}

/**
 * Request gateway.
 * 
 * @param string $command
 * @param array $data
 * 
 * @return object
 */
function _sms_emay_gateway_request($command, $data = array()) {
  $gateway = sms_gateways('gateways');
  $config = $gateway['sms_emay']['configuration'];

  $data += array(
    'cdkey' => $config['sms_emay_cdkey'],
    'password' => $config['sms_emay_password'],
  );

  $params = drupal_http_build_query($data);
  $url = $config['sms_emay_url'] . $command . '?' . $params;
  $result = drupal_http_request($url, array('method'=>'GET', 'timeout'=>10));
  
  watchdog('sms_emay', $url . '<br /><pre>' . print_r($result, TRUE) . '</pre>');

  return $result;
}
